using UnityEngine;
using System.Collections;

public class gameController : MonoBehaviour {

	public bool playerMove = true;
	static int plyRange = 4;

	public int[,] officialGameboard = new int[8, 8];
	int timer = 0;

	bool end = false;

	GameObject tempObject;
	tokenBehavior realObject;

	GameObject guiObject;
	GUIText endGameGui;

	// Use this for initialization
	void Start () {
		guiObject = GameObject.Find ("EndGame");
		endGameGui = guiObject.gameObject.GetComponent<GUIText> ();
		endGameGui.enabled = false;

		//initialize the board
		for(int i = 0; i < 8; i++){
			for(int j = 0; j < 8; j++){
				officialGameboard[i, j] = 0;
			}
		}

		//the starting game state
		officialGameboard [3, 3] = 2;
		officialGameboard [4, 4] = 2;
		officialGameboard [3, 4] = 1;
		officialGameboard [4, 3] = 1;

	}
	
	// Update is called once per frame
	void Update () {
		timer++;

		if(!end){
			if (playerMove){
				timer = 0;
				getValid(playerMove);
			}
			else if(timer == 120){
				startAlphaBeta();
			}
		}

		getScore ();
	}

	void startAlphaBeta(){

		int[] tempArray;
		tempArray = getValid (false, officialGameboard);
		int[] validMoves = new int[tempArray.Length];
		tempArray.CopyTo (validMoves, 0);

		if(validMoves == null)
			endGame();
	
		int tempChildScore = -64;
		int midway = validMoves.Length / 2;
		int maxIndex = 0;
		int max = -64;

		for(int i = 0; i < midway; i++){
			tempChildScore = AlphaBeta(officialGameboard, plyRange, validMoves[i], validMoves[i+midway], -64, 64, false);
			if(tempChildScore > max){
				max = tempChildScore;
				maxIndex = i;
			}
		}

		string objectName = validMoves[maxIndex].ToString () + validMoves[maxIndex+midway].ToString ();
		tempObject = GameObject.Find(objectName);
		realObject = tempObject.gameObject.GetComponent<tokenBehavior> ();
		realObject.setAsActive (validMoves [maxIndex], validMoves [maxIndex+midway], false);
		playerMove = true;
	}

	int AlphaBeta(int[,] gameState, int ply, int row, int col, int alpha, int beta, bool player){
		int[,] temporaryBoardState = new int[8, 8];
		int value;
		int thisAlpha = -64;
		int thisBeta = 64;

		for(int i = 0; i<8; i++){
			for(int j = 0; j < 8; j++){
				temporaryBoardState[i, j] = gameState[i, j];
			}
		}

		if(player == true){

			temporaryBoardState[row, col] = 2;

			value = -64;
			if(ply == 0){
				value = evaluateH(temporaryBoardState, true);
				return value;
			}
			else{
				int[] originalCopy;
				originalCopy = getValid(!player, gameState);

				int[] validMoveScores = new int[originalCopy.Length];
				originalCopy.CopyTo(validMoveScores, 0);

				int midway = validMoveScores.Length/2;
				for(int i = 0; i < midway; i++){
					value = AlphaBeta(temporaryBoardState, ply-1, validMoveScores[i], validMoveScores[i+midway], thisAlpha, thisBeta, !player);
					if(value > beta)
						break;
					if(value > thisAlpha)
						thisAlpha = value;
					if(value < thisBeta)
						thisBeta = value;
				}
				return thisAlpha;
			}
		}
		else{
			temporaryBoardState[row, col] = 1;
			
			value = 64;
			if(ply == 0){
				value = evaluateH(temporaryBoardState, true);
				return value;
			}
			else{
				int[] originalCopy;
				originalCopy = getValid(player, gameState);
				
				int[] validMoveScores = new int[originalCopy.Length];
				originalCopy.CopyTo(validMoveScores, 0);
				
				int midway = validMoveScores.Length/2;
				for(int i = 0; i < midway; i++){
					value = AlphaBeta(temporaryBoardState, ply-1, validMoveScores[i], validMoveScores[i+midway], thisAlpha, thisBeta, !player);
					if(value < alpha)
						break;
					if(value > thisAlpha)
						thisAlpha = value;
					if(value < thisBeta)
						thisBeta = value;
				}
				return thisBeta;
			}

		}
	}

	int evaluateH(int[,] gameState, bool minOrMax){
		int hValue = 0;
		if(minOrMax == true){
			for(int i = 0; i < 8; i++){
				for(int j = 0; j < 8; j++){
					if(gameState[i, j] == 2)
						hValue++;
					else if(gameState[i, j] == 1)
						hValue--;
				}
			}
		} 
		else{
			for(int i = 0; i < 8; i++){
				for(int j = 0; j < 8; j++){
					if(gameState[i, j] == 1)
						hValue++;
					else if(gameState[i, j] == 2)
						hValue--;
				}
			}
		}

		return hValue;
	}

	void getValid(bool playerOrComp){

		for(int i = 0; i < 8; i++){
			for(int j = 0; j < 8; j++){
				if(officialGameboard[i, j] == 2)
					getValidMoves(i, j, 2, 1);
			}
		}
		isEnd();

		return;
	}

	void getValidMoves(int checkRow, int checkCol, int mine, int their){
		//iterator will check the direct pieces
		bool first = true;
		
		//check left up diagonal
		int up = checkRow;
		for(int left = checkCol-1; left > -1; left--){
			if(up+1 < 8)
				up++;
			else
				goto left;

			if(!first && officialGameboard[up, left] == mine){
				goto left;
			}
			else if(!first && officialGameboard[up, left] == 0){
				setValid(up, left);
				goto left;
			}
			else if(first && officialGameboard[up, left] == their){
				first = false;
			}
			else if(first && officialGameboard[up, left] != their)
				goto left;
		}

		
	left:	
		first = true;
		for(int left = checkCol-1; left > -1; left--){
			if(!first && officialGameboard[checkRow, left] == mine){
				goto leftDown;
			}
			else if(!first && officialGameboard[checkRow, left] == 0){
				setValid(checkRow, left);
				goto leftDown;
			}
			else if(first && officialGameboard[checkRow, left] == their){
				first = false;
			}
			else if(first && officialGameboard[checkRow, left] != their){
				goto leftDown;
			}
		}
		
	leftDown:
		first = true;
		int down = checkRow;
		for(int left = checkCol-1; left > -1; left--){
			if(down - 1  > -1) 
				down--;
			else
				goto down;

			if(!first && officialGameboard[down, left] == mine){
				goto down;
			}
			else if(!first && officialGameboard[down, left] == 0){
				setValid(down, left);
				goto down;
			}
			else if(first && officialGameboard[down, left] == their){
				first = false;
			}
			else if(first && officialGameboard[down, left] != their){
				goto down;
			}
		}
	
		
	down:
		first = true;
		for(down = checkRow-1; down > -1; down--){
			if(!first && officialGameboard[down, checkCol] == mine){
				goto rightDown;
			}
			else if(!first && officialGameboard[down, checkCol] == 0){
				setValid(down, checkCol);
				goto rightDown;
			}
			else if(first && officialGameboard[down, checkCol] == their){
				first = false;
			}
			else if(first && officialGameboard[down, checkCol] != their){
				goto rightDown;
			}
		}
		
	rightDown:
		first = true;
		down = checkRow; 
		for(int right = checkCol+1; right < 8; right++){
			if(down - 1  > -1)
				down--;
			else
				goto rightD;

			if(!first && officialGameboard[down, right] == mine){
				goto rightD;
			}
			else if(!first && officialGameboard[down, right] == 0){
				setValid(down, right);
				goto rightD;
			}
			else if(first && officialGameboard[down, right] == their){
				first = false;
			}
			else if(first && officialGameboard[down, right] != their){
				goto rightD;
			}
		}
	
	rightD:
			first = true;
			for(int right = checkCol+1; right < 8; right++){
				if(!first && officialGameboard[checkRow, right] == mine){
					goto rightUp;
				}
				else if(!first && officialGameboard[checkRow, right] == 0){
					setValid(checkRow, right);
					goto rightUp;
				}
				else if(first && officialGameboard[checkRow, right] == their){
					first = false;
				}
				else if(first && officialGameboard[checkRow, right] != their){
					goto rightUp;
				}
			}
	rightUp:
		first = true;
		up = checkRow;
		for(int right = checkCol+1; right < 8; right++){
			if(up+1 < 8)
				up++;
			else
				goto up;
				
			if(!first && officialGameboard[up, right] == mine){
				goto up;
			}
			else if(!first && officialGameboard[up, right] == 0){
				setValid(up, right);
				goto up;
			}
			else if(first && officialGameboard[up, right] == their){
				first = false;
			}
			else if(first && officialGameboard[up, right] != their){
				goto up;
			}
		}

	up:
		first = true;
		for(up = checkRow+1; up < 8; up++){
			if(!first && officialGameboard[up, checkCol] == mine){
				goto done;
			}
			else if(!first && officialGameboard[up, checkCol] == 0){
				setValid(up, checkCol);
				goto done;
			}
			else if(first && officialGameboard[up, checkCol] == their){
				first = false;
			}
			else if(first && officialGameboard[up, checkCol] != their){
				goto done;
			}
		}
		
	done: return;
	}

	int[] getValid(bool compOrPlayer, int[,] whichBoard){
		ArrayList validRow = new ArrayList ();
		ArrayList validCol = new ArrayList ();

		if(compOrPlayer){
			for(int i = 0; i < 8; i++){
				for(int j = 0; j < 8; j++){
					if(whichBoard[i, j] == 2){
						bool first = true;
						
						//check left up diagonal
						int up = i;
						for(int left = j-1; left > -1; left--){
							if(up+1 < 8)
								up++;
							else
								goto left;

							if(!first && whichBoard[up, left] == 2)
								goto left;
							else if(!first && whichBoard[up, left] == 0){
								validRow.Add(up);
								validCol.Add(left);
								goto left;
							}
							else if(first && whichBoard[up, left] == 1){
								first = false;
							}
							else if(first && whichBoard[up, left] != 1)
								goto left;
							}

						
					left:	
						first = true;
						for(int left = j-1; left > -1; left--){
							if(!first && whichBoard[i, left] == 2)
								goto leftDown;
							else if(!first && whichBoard[i, left] == 0){
								validRow.Add(i);
								validCol.Add(left);
								goto leftDown;
							}
							else if(first && whichBoard[i, left] == 1){
								first = false;
							}
							else if(first && whichBoard[i, left] != 1)
								goto leftDown;
						}
						
					leftDown:
						first = true;
						int down = i;
						for(int left = j-1; left > -1; left--){
							if(down - 1  > -1) 
								down--;
							else
								goto down;

							if(!first && whichBoard[down, left] == 2){
								goto down;
							}
							else if(!first && whichBoard[down, left] == 0){
								validRow.Add(down);
								validCol.Add(left);
								goto down;
							}
							else if(first && whichBoard[down, left] == 1){
								first = false;
							}
							else if(first && whichBoard[down, left] != 1){
								goto down;
							}
						}
						
					down:
							first = true;
						for(down = i-1; down > -1; down--){
							if(!first && whichBoard[down, j] == 2){
								goto rightDown;
							}
							else if(!first && whichBoard[down, j] == 0){
								validRow.Add(down);
								validCol.Add(j);
								goto rightDown;
							}
							else if(first && whichBoard[down, j] == 1){
								first = false;
							}
							else if(first && whichBoard[down, j] != 1){
								goto rightDown;
							}
						}
						
					rightDown:
							first = true;
						down = i;
						for(int right = j+1; right < 8; right++){
							if(down - 1  > -1)
								down--;
							else
								goto rightD;
								
							if(!first && whichBoard[down, right] == 2){
								goto rightD;
							}
							else if(!first && whichBoard[down, right] == 0){
								validRow.Add(down);
								validCol.Add(right);
								goto rightD;
							}
							else if(first && whichBoard[down, right] == 1){
								first = false;
							}
							else if(first && whichBoard[down, right] != 1){
								goto rightD;
							}
						}

					rightD:
							first = true;
						for(int right = j+1; right < 8; right++){
							if(!first && whichBoard[i, right] == 2){
								goto rightUp;
							}
							else if(!first && whichBoard[i, right] == 0){
								validRow.Add(right);
								validCol.Add(j);
								goto rightUp;
							}
							else if(first && whichBoard[i, right] == 1){
								first = false;
							}
							else if(first && whichBoard[i, right] != 1){
								goto rightUp;
							}
						}
					rightUp:
							first = true;
						up = i;
						for(int right = j+1; right < 8; right++){
							if(up+1 < 8)
								up++;
							else
								goto up;

							if(!first && whichBoard[up, right] == 2){
								goto up;
							}
							else if(!first && whichBoard[up, right] == 0){
								validRow.Add(up);
								validCol.Add(right);
								goto up;
							}
							else if(first && whichBoard[up, right] == 1){
								first = false;
							}
							else if(first && whichBoard[up, right] != 1){
								goto up;
							}
						}
					up:
							first = true;
						for(up = i+1; up < 8; up++){
							if(!first && whichBoard[up, j] == 2){
								goto done;
							}
							else if(!first && whichBoard[up, j] == 0){
								validRow.Add(up);
								validCol.Add(j);
								goto done;
							}
							else if(first && whichBoard[up, j] == 1){
								first = false;
							}
							else if(first && whichBoard[up, j] != 1){
								goto done;
							}
						}

						
					done: first = true;
					}
				}
			}

			for(int index = 0; index < validCol.Count; index++)
				validRow.Add(validCol[index]);
			
			int[] validPoints = (int[]) validRow.ToArray(typeof(int));


			return validPoints;
		}
		else
		{
			for(int i = 0; i < 8; i++){
				for(int j = 0; j < 8; j++){
					if(whichBoard[i, j] == 1){
						bool first = true;
						
						//check left up diagonal
						int up = i;
						for(int left = j-1; left > -1; left--){
							if(up+1 < 8)
								up++;
							else
								goto left;
								
							if(!first && whichBoard[up, left] == 1)
								goto left;
							else if(!first && whichBoard[up, left] == 0){
								validRow.Add(up);
								validCol.Add(left);
								goto left;
							}
							else if(first && whichBoard[up, left] == 2)
								first = false;
							else if(first && whichBoard[up, left] != 2)
								goto left;
						}

						
					left:	
						first = true;
						for(int left = j-1; left > -1; left--){
							if(!first && whichBoard[i, left] == 1)
								goto leftDown;
							else if(!first && whichBoard[i, left] == 0){
								validRow.Add(i);
								validCol.Add(left);
								goto leftDown;
							}
							else if(first && whichBoard[i, left] == 2)
								first = false;
							else if(first && whichBoard[i, left] != 2)
								goto leftDown;
						}
						
					leftDown:
						first = true;
						int down = i;
						for(int left = j-1; left > -1; left--){
							if(down - 1  > -1) 
								down--;
							else
								goto down;

							if(!first && whichBoard[down, left] == 1){
								goto down;
							}
							else if(!first && whichBoard[down, left] == 0){
								validRow.Add(down);
								validCol.Add(left);
								goto down;
							}
							else if(first && whichBoard[down, left] == 2){
								first = false;
							}
							else if(first && whichBoard[down, left] != 2){
								goto down;
							}
						}

						
					down:
						first = true;
						for(down = i-1; down > -1; down--){
							if(!first && whichBoard[down, j] == 1){
								goto rightDown;
							}
							else if(!first && whichBoard[down, j] == 0){
								validRow.Add(down);
								validCol.Add(j);
							goto rightDown;
							}
							else if(first && whichBoard[down, j] == 2){
								first = false;
							}
							else if(first && whichBoard[down, j] != 2){
								goto rightDown;
							}
						}
						
					rightDown:
						first = true;
						down = i; 
						for(int right = j+1; right < 8; right++){
							if(down - 1  > -1)
								down--;
							else
								goto rightD;

							if(!first && whichBoard[down, right] == 1){
								goto rightD;
							}
							else if(!first && whichBoard[down, right] == 0){
								validRow.Add(down);
								validCol.Add(right);
								goto rightD;
							}
							else if(first && whichBoard[down, right] == 2){
								first = false;
							}
							else if(first && whichBoard[down, right] != 2){
								goto rightD;
							}
						}

					rightD:
							first = true;
						for(int right = j+1; right < 8; right++){
							if(!first && whichBoard[i, right] == 1){
								goto rightUp;
							}
							else if(!first && whichBoard[i, right] == 0){
								validRow.Add(i);
								validCol.Add(right);
								goto rightUp;
							}
							else if(first && whichBoard[i, right] == 2){
								first = false;
							}
							else if(first && whichBoard[i, right] != 2){
								goto rightUp;
							}
						}
					rightUp:
						first = true;
						up = i; 
						for(int right = j+1; right < 8; right++){
							if(up+1 < 8)
								up++;
							else
								goto up;

							if(!first && whichBoard[up, right] == 1){
								goto up;
								}
							else if(!first && whichBoard[up, right] == 0){
								validRow.Add(up);
								validCol.Add(right);
								goto up;
							}
							else if(first && whichBoard[up, right] == 2){
								first = false;
							}
							else if(first && whichBoard[up, right] != 2){
								goto up;
							}
						}

					up:
							first = true;
						for(up = i+1; up < 8; up++){
							if(!first && whichBoard[up, j] == 1){
								goto done;
							}
							else if(!first && whichBoard[up, j] == 0){
								validRow.Add(up);
								validCol.Add(j);
								goto done;
							}
							else if(first && whichBoard[up, j] == 2){
								first = false;
							}
							else if(first && whichBoard[up, j] != 2){
								goto done;
							}
						}
						
						
					done: first = true;
					}
				}
			}
			
			for(int index = 0; index < validCol.Count; index++)
				validRow.Add(validCol[index]);

			int[] validPoints = (int[]) validRow.ToArray(typeof(int));
			return validPoints;
		}
	}

	void setValid(int validRow, int validCol){

		string objectName = validRow.ToString () + validCol.ToString ();

		try{
			tempObject = GameObject.Find(objectName);
			realObject = tempObject.gameObject.GetComponent<tokenBehavior>();
			realObject.validMove = true;
		}
		catch{
			Debug.Log(objectName);
		}


	}


	public void resetValidCondition(){
		for(int i = 0; i < 8; i++){
			for(int j = 0; j < 8; j++){
				if(officialGameboard[i, j] == 0){
					string objectName = i.ToString () + j.ToString ();
					tempObject = GameObject.Find(objectName);
					realObject = tempObject.gameObject.GetComponent<tokenBehavior>();
					realObject.validMove = false;
				}
			}
		}
	}

	public void newPieces(int currRow, int currCol, int mine, int their){
		ArrayList tempRow = new ArrayList();
		ArrayList tempCol = new ArrayList();

		bool first = true;

		//check left up diagonal
		int up = currRow;
		for(int left = currCol-1; left > -1; left--){
			if(up+1 < 8)
				up++;
			else
				goto left;

			if(!first && officialGameboard[up, left] == mine){
				revertPieces(tempRow, tempCol);
				goto left;
			}
			else if(!first && officialGameboard[up, left] == 0){
				goto left;
			}
			else if(officialGameboard[up, left] == their){
				tempRow.Add(up);
				tempCol.Add(left);
				first = false;
			}
			else if(first && officialGameboard[up, left] != their  )
				goto left;
		}
	

	left:	
		tempRow.Clear();
		tempCol.Clear();

		first = true;
		for(int left = currCol-1; left > -1; left--){
			if(!first && officialGameboard[currRow, left] == mine){
				revertPieces(tempRow, tempCol);
				goto leftDown;
			}
			else if(!first && officialGameboard[currRow, left] == 0){
				goto leftDown;
			}
			else if(officialGameboard[currRow, left] == their){
				tempRow.Add(currRow);
				tempCol.Add(left);
				first = false;
			}
			else if(first && officialGameboard[currRow, left] != their){
				goto leftDown;
			}
		}

leftDown:
		tempRow.Clear();
		tempCol.Clear();

		first = true;
		int down = currRow;
		for(int left = currCol-1; left > -1; left--){
			if(down - 1  > -1) 
				down--;
			else
				goto down;
				if(!first && officialGameboard[down, left] == mine){
				revertPieces(tempRow, tempCol);
				goto down;
			}
			else if(!first && officialGameboard[down, left] == 0){
				goto down;
			}
			else if(officialGameboard[down, left] == their){
				tempRow.Add(down);
				tempCol.Add(left);
				first = false;
			}
			else if(first && officialGameboard[down, left] != their){
				goto down;
			}
		}


down:
		tempRow.Clear();
		tempCol.Clear();

		first = true;
		for(down = currRow-1; down > -1; down--){
			if(!first && officialGameboard[down, currCol] == mine){
				revertPieces(tempRow, tempCol);
				goto rightDown;
			}
			else if(!first && officialGameboard[down, currCol] == 0){
				goto rightDown;
			}
			else if(officialGameboard[down, currCol] == their){
				tempRow.Add(down);
				tempCol.Add(currCol);
				first = false;
			}
			else if(first && officialGameboard[down, currCol] != their){
				goto rightDown;
			}
		}

rightDown:
		tempRow.Clear();
		tempCol.Clear();

		first = true;
		down = currRow;
		for(int right = currCol+1; right < 8; right++){
			if(down - 1  > -1)
				down--;
			else

			goto rightD;
			if(!first && officialGameboard[down, right] == mine){
				revertPieces(tempRow, tempCol);
				goto rightD;
			}
			else if(!first && officialGameboard[down, right] == 0){
				goto rightD;
			}
			else if(officialGameboard[down, right] == their){
				tempRow.Add(down);
				tempCol.Add (right);
				first = false;
			}
			else if(first && officialGameboard[down, right] != their){
				goto rightD;
			}
		}

rightD:
		tempRow.Clear();
		tempCol.Clear();

		first = true;
		for(int right = currCol+1; right < 8; right++){
			if(!first && officialGameboard[currRow, right] == mine){
				revertPieces(tempRow, tempCol);
				goto rightUp;
			}
			else if(!first && officialGameboard[currRow, right] == 0){
				goto rightUp;
			}
			else if(officialGameboard[currRow, right] == their){
				tempRow.Add(currRow);
				tempCol.Add(right);
				first = false;
			}
			else if(first && officialGameboard[currRow, right] != their){
				goto rightUp;
			}
		}
rightUp:
		tempRow.Clear();
		tempCol.Clear();

		first = true;
		up = currRow; 
		for(int right = currCol+1; right < 8; right++){
			if(up+1 < 8)
				up++;
			else
				goto up;
			if(!first && officialGameboard[up, right] == mine){
				revertPieces(tempRow, tempCol);
				goto up;
			}
			else if(!first && officialGameboard[up, right] == 0){
				goto up;
			}
			else if(officialGameboard[up, right] == their){
				tempRow.Add(up);
				tempCol.Add(right);
				first = false;
			}
			else if(first && officialGameboard[up, right] != their){
				goto up;
			}
		}
	
up:
		tempRow.Clear();
		tempCol.Clear();

		first = true;
		for(up = currRow+1; up < 8; up++){
			if(!first && officialGameboard[up, currCol] == mine){
				revertPieces(tempRow, tempCol);
				goto done;
			}
			else if(!first && officialGameboard[up, currCol] == 0){
				goto done;
			}
			else if(officialGameboard[up, currCol] == their){
				tempRow.Add(up);
				tempCol.Add(currCol);
				first = false;
			}
			else if(first && officialGameboard[up, currCol] != their){
				goto done;
			}
		}
	done:	
		tempCol.Clear();
		tempRow.Clear();	
	}

	void revertPieces(ArrayList convertRows, ArrayList convertCol){
		for(int iter = 0; iter < convertRows.Count; iter++){
			string objectName = convertRows[iter].ToString() + convertCol[iter].ToString();
			tempObject = GameObject.Find(objectName);
			realObject = tempObject.gameObject.GetComponent<tokenBehavior>();
			realObject.changeColor();
		}
	}

	void endGame(){

		end = true;

		endGameGui.enabled = true;

		for(int i = 0; i < 8; i++){
			for(int j = 0; j < 8; j++){
				string objectName = i.ToString() + j.ToString();
				tempObject = GameObject.Find(objectName);
				realObject = tempObject.gameObject.GetComponent<tokenBehavior>();
				Destroy(realObject);
			}
		}
	}

	void isEnd(){
		bool wantToEnd = true;

		for(int i = 0; i < 8; i++){
			for(int j = 0; j < 8; j++){
				string objectName = i.ToString() + j.ToString();
				tempObject = GameObject.Find(objectName);
				realObject = tempObject.gameObject.GetComponent<tokenBehavior>();
				if(realObject.validMove)
					wantToEnd = false;
			}
		}

		if(wantToEnd)
			endGame();
	}

	void getScore(){
		int playerScore = 0;
		int compScore = 0;

		for(int i = 0; i < 8; i++){
			for(int j = 0; j < 8; j++){
				if(officialGameboard[i, j] == 2)
					playerScore++;
				else if(officialGameboard[i, j] == 1)
					compScore++;
			}
		}

		GameObject pScoreObject;
		GUIText pScore;

		GameObject cScoreObject;
		GUIText cScore;

		pScoreObject = GameObject.Find ("pScore");
		pScore = pScoreObject.gameObject.GetComponent<GUIText> ();
		pScore.GetComponent<GUIText>().text = playerScore.ToString ();

		cScoreObject = GameObject.Find ("cScore");
		cScore = cScoreObject.gameObject.GetComponent<GUIText> ();
		cScore.GetComponent<GUIText>().text = compScore.ToString ();
	}
}
