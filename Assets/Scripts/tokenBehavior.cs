using UnityEngine;
using System.Collections;

public class tokenBehavior : MonoBehaviour {

	public bool tokenColor = true;
	public bool isVisible;
	public bool validMove;

	public int row, col;

	GameObject gameControllerObject;
	gameController gameScript;

	// Use this for initialization
	void Start () {
		if (tokenColor)
						gameObject.GetComponent<Renderer>().material.color = Color.white;
				else
						gameObject.GetComponent<Renderer>().material.color = Color.gray;

		if (isVisible)
			GetComponent<Renderer>().enabled = true;
		else
			GetComponent<Renderer>().enabled = false;

		gameControllerObject = GameObject.FindWithTag ("GameController");
		gameScript = gameControllerObject.gameObject.GetComponent<gameController> ();
	}

	void OnMouseDown(){
		if (gameScript.playerMove && validMove) {
			this.GetComponent<Renderer>().enabled = true;
			this.gameObject.GetComponent<Renderer>().material.color = Color.gray;

			gameScript.officialGameboard[row, col] = 2;

			gameScript.resetValidCondition();
			gameScript.newPieces(row, col, 2, 1);

			gameScript.playerMove = false;
			this.validMove = !validMove;
		}

	}

	public void changeColor(){
		if (gameObject.GetComponent<Renderer>().material.color == Color.white)
			gameObject.GetComponent<Renderer>().material.color = Color.gray;
		else
			gameObject.GetComponent<Renderer>().material.color = Color.white;

		if(gameScript.officialGameboard[row, col] == 1)
			gameScript.officialGameboard[row, col] = 2;
		else
			gameScript.officialGameboard[row, col] = 1;
	}

	public void setAsActive(int checkRow, int checkCol, bool playerMove){
		gameScript.resetValidCondition ();

		this.GetComponent<Renderer>().enabled = true;

		if(playerMove){
			gameScript.officialGameboard [checkRow, checkCol] = 2;
			this.GetComponent<Renderer>().material.color = Color.gray;
			gameScript.newPieces(checkRow, checkCol, 2, 1);
		}
		else{
			gameScript.officialGameboard[checkRow, checkCol] = 	1;
			this.GetComponent<Renderer>().material.color = Color.white;
			gameScript.newPieces(checkRow, checkCol, 1, 2);
		}

		this.validMove = false;
	}
}
